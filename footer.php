<footer class="section-p1">
        <div class="col">
          <h4>Music Premium</h4>
          <a href="shop.php">Trgovina</a>
          <a href="otkup.php">Otkup</a>
          <a href="about.php">O nama</a>
        </div>

        <div class="col">
          <h4>Kontakt</h4>
          <p><strong>Adresa: </strong>Ul. Kneza Trpimira 2B, Osijek</p>
          <p><strong>Web stranica: </strong>ferit.unios.hr</p>
          <p><strong>Telefon: </strong>031/495-400</p>
        </div>

        <div class="col">
          <h4>Radno vrijeme</h4>
          <p>Ponedjeljak - petak</p>
          <p>10:00 - 18:00 h</p><br>
          <p>Subota</p>
          <p>10:00 - 14:00 h</p>
        </div>
</footer>