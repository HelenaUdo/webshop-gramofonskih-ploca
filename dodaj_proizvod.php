<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
        <title>Webshop gramofonskih ploca</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

        <style>
            table, th, td {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table {
                width: 70%;
                border-collapse: collapse;
                font-size: 20px;
                padding-left: 20px;
                
            }
            table th{
                padding-top: 8px;
                padding-bottom: 8px;
                text-align: left;
                background-color: brown;
                color: white;
            }    
        </style>

    </head>

    <body>
      <section id="firsthead">
        <div>
          <ul id="firstnavbar">
            <li><a href="odjava.php">    Odjava</a></li>
          </ul>
        </div>
      </section>

      <section id="header">
        <a href="#"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="indexZaLog.php">NASLOVNA</a></li>
            <li><a href="otkup_admin.php">OTKUP</a></li>
            <li><a class="active" href="dodaj_proizvod.php">DODAJ PROIZVOD</a></li>
          </ul>
        </div>
      </section>

      <?php

      if($_POST){
      include "spoj.php";
      $naziv=$_POST["Naziv"];
      $cijenakune=$_POST["CijenaKune"];
      $cijenaeuri=$_POST["CijenaEuri"];
      $opis=$_POST["Opis"];
      $slika=$_POST["Slika"];
        $sql= "INSERT INTO proizvodi (Naziv,CijenaKune,CijenaEuri,Opis,Slika) VALUES ('$naziv', '$cijenakune', '$cijenaeuri', '$opis', '$slika')";
      if($conn -> query($sql)===TRUE){
        echo "
					<div class='alert alert-success'>
						<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
						<b>Proizvod je dodan</b>
					</div>";
      }else{
        echo "Error pri dodavanju rekorda" .$conn -> error;
      }
      $conn -> close();
      }

      ?>

      <section id="otkup_unos" class="section-p1">
        <form action="" method="post">
            <span>UNOS PROIZVODA</span>
            <input type="text" name="Naziv" id="Naziv" placeholder="Naziv">
            <input type="number" name="CijenaKune" id="CijenKune" placeholder="Cijena u kunama">
            <input type="number" name="CijenaEuri" id="CijenaEuri" placeholder="Cijena u eurima">
            <input type="text" name="Opis" id="Opis" placeholder="Opis">
            <input type="file" id="Slika" name="Slika" placeholder="Slika">
            <button type="submit" class="send_normal" id="otkupise">Dodaj</button><br><br>
        </form>
      </section>

      <div style="padding-left: 40px;">

        <?php
            include "spoj.php";
            if(!empty($_SESSION['prijavljen']) && $_SESSION['prijavljen'] == true){
                    if($_SESSION['uloga'] == 'admin'){
                        echo "<br><p style='font-size:20px'>" ."ISPIS DODANIH PLOČA: ". "</p>";
                    }    
                        else{
                        echo "Prijavljeni ste kao kupac.";            
                    }                
            } else{
                header("Location: login.php");
                }
                $sql = "SELECT Naziv,CijenaKune,CijenaEuri,Opis,Slika FROM proizvodi";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    echo "<table><tr><th> NAZIV </th><th> CIJENA U KUNAMA </th><th> CIJENA U EURIMA </th><th> OPIS </th></tr>";
                    while($row = mysqli_fetch_assoc($result)) {
                        echo  "<tr> ";
                        echo "<tr><td>" .$row['Naziv']. "</td><td>" .$row['CijenaKune']. "</td><td>" .$row['CijenaEuri']. "</td><td>" .$row['Opis']. "</td></tr>";
                        echo "</tr> ";
                    }
                    echo "</table>";
                } else {
                echo "0 results";
                }
                mysqli_close($conn);

            ?>
      </div><br>

    </body>

</html>


    


      


