<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Webshop gramofonskih ploca</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

    </head>

    <body>
      <section id="firsthead">
        <div>
          <ul id="firstnavbar">
            <li><a href="register.php">Registracija</a></li>
            <li><a href="login.php">Prijava</a></li>
          </ul>
        </div>
      </section>

      <section id="header">
        <a href="#"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="index.php">NASLOVNA</a></li>
            <li><a class="active" href="shop.php">TRGOVINA</a></li>
            <li><a href="otkup.php">OTKUP</a></li>
            <li><a href="about.php">O NAMA</a></li>
            <li class="cart"><a href="cart.php"><i class="far fa-shopping-bag"></i><span> 0</span></a></li>
          </ul>
        </div>
      </section>

      <section id="onePro" class="section-p1">
        <div class="oneProImage">
            <img src="images/johnnycash.jpg" width="100%" id="MainImg" alt="">
        </div>

        <div class="oneProDescription">
            <h6>Trgovina/Ploče</h6>
            <h2>Johnny Cash-The Mighty Johnny Cash</h2>
            <h4>150kn<small class="text-secondary"> (19.91€)</small></h4>
            <button class="normal">DODAJ U KOŠARICU</button>
            <br><br><h4>Opis</h4>
            <span>
              Žanr: Country, Folk, World<br>
              Stanje omota: VG<br>
              Stanje ploče: VG<br>

              *VG su ploče sa znakovima korištenja i reproduciranja od strane prethodnog vlasnika koji se dobro brinuo o njima.
              Površina ploče može pokazivati neke znakove trošenja ili imati lagane ogrebotine koje ne utječu na zvuk. 
              Omot ploče može imati znakove manjeg trošenja, blago okrenute kutove, lagani rascjep na spojevima ili izreze.
            </span><br><br>
            <span>Poslušajte izdvojenu pjesmu iz asortimana: "I Got Stripes"</span><br>
            <audio controls>
              <source src="audio/JohnnyC.mp3" type="audio/ogg">
              <source src="audio/JohnnyC.mp3" type="audio/mpeg">
            Your browser does not support the audio element.
            </audio>
        </div>
      </section>

      <?php
      include "footer.php";
      ?>

      

      <script src="script.js"></script>
    </body>


</html>