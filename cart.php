<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Košarica</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

    </head>

    <body>
    <?php
            include "spoj.php";
            session_start();
            if ($_SESSION == NULL) {
        ?>
        <section id="firsthead">
          <div>
            <ul id="firstnavbar">
              <li><a href="register.php">Registracija</a></li>
              <li><a href="login.php">Prijava</a></li>
            </ul>
          </div>
        </section>
        <?php  
        } else {
        ?>
        <section id="firsthead">
          <div>
              <ul id="firstnavbar">
                <li><a href="odjava.php">Odjava</a></li>
              </ul>
          </div>
        </section>
        <?php
        }
        ?>

      <section id="header">
        <a href="index.php"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="index.php">NASLOVNA</a></li>
            <li><a href="shop.php">TRGOVINA</a></li>
            <li><a href="otkup.php">OTKUP</a></li>
            <li><a href="about.php">O NAMA</a></li>
            <li id="lgbag"><a class="active" href="cart.php"><i class="far fa-shopping-bag"></i><span> 0</span></a></li>
          </ul>
        </div>
      </section>

    </body>
</html>