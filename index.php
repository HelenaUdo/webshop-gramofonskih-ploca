<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Webshop gramofonskih ploca</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

    </head>

    <body>
        <?php
            include "spoj.php";
            session_start();
            if ($_SESSION == NULL) {
        ?>
        <section id="firsthead">
          <div>
            <ul id="firstnavbar">
              <li><a href="register.php">Registracija</a></li>
              <li><a href="login.php">Prijava</a></li>
            </ul>
          </div>
        </section>
        <?php  
        } else {
        ?>
        <section id="firsthead">
          <div>
              <ul id="firstnavbar">
                <li><a href="odjava.php">Odjava</a></li>
              </ul>
          </div>
        </section>
        <?php
        }
        ?>

      <section id="header">
        <a href="index.php"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a class="active" href="index.php">NASLOVNA</a></li>
            <li><a href="shop.php">TRGOVINA</a></li>
            <li><a href="otkup.php">OTKUP</a></li>
            <li><a href="about.php">O NAMA</a></li>
            <li id="lgbag"><a href="cart.php"><i class="far fa-shopping-bag"></i><span> 0</span></a></li>
            <a href="#" id="closeit"><i class="far fa-times"></i></a>
          </ul>
        </div>

        <div id="mobile">
          <i id="bar" class="fas fa-outdent"></i>
        </div>
      </section>

      <section id="inside">
      </section>

      <section id="poznatiIzv" class="section-p1">
        <h2>POZNATI IZVOĐAČI</h2>
        <div class="pro-container">
          <div class="pro" onclick="window.location.href='one_item.php'">
            <img src="images/johnnycash.jpg" alt="">
            <div class="opis">
              <h4>Johnny Cash-The Mighty Johnny Cash</h4>
              <h5>150kn<small class="text-secondary"> (19.91€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>

          <div class="pro" onclick="window.location.href='sinatra_item.php'">
            <img src="images/sinatra.jpg" alt="">
            <div class="opis">
              <h4>Frank Sinatra-A Jolly Christmas From Frank Sinatra</h4>
              <h5>160kn<small class="text-secondary"> (21.24€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>

          <div class="pro" onclick="window.location.href='berlin_item.php'">
            <img src="images/berlin.jpg" alt="">
            <div class="opis">
              <h4>Berlin-Count Three And Pray</h4>
              <h5>170kn<small class="text-secondary"> (22.56€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>
        </div>
      </section>


      <section id="poznatiIzv" class="section-p1">
        <h2>NOVO U PONUDI</h2>
        <div class="pro-container">
          <div class="pro">
            <img src="images/mia.jpg" alt="">
            <div class="opis">
              <h4>Mia Dimšić-Život Nije Siv</h4>
              <h5>90kn<small class="text-secondary"> (11.95€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>

          <div class="pro">
            <img src="images/buble.jpg" alt="">
            <div class="opis">
              <h4>Michael Buble-Christmas</h4>
              <h5>160kn<small class="text-secondary"> (21.24€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>

          <div class="pro">
            <img src="images/opca.jpg" alt="">
            <div class="opis">
              <h4>Opća opasnost-Karta Do Prošlosti</h4>
              <h5>100kn<small class="text-secondary"> (13.27€)</small></h5>
              <button class="normal">ODABERI</button>
            </div>
          </div>
        </div>
      </section><br>

      <?php
      include "footer.php";
      ?>
      
      <script src="script.js"></script>

    </body>

</html>