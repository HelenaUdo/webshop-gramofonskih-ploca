<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>OtkupZaLog</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

        <style>
            table, th, td {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table {
                width: 70%;
                border-collapse: collapse;
                font-size: 20px;
                padding-left: 20px;
                
            }
            table th{
                padding-top: 8px;
                padding-bottom: 8px;
                text-align: left;
                background-color: brown;
                color: white;
            }    
        </style>
    </head>

    <body>
      <section id="firsthead">
        <div>
          <ul id="firstnavbar">
            <li><a href="odjava.php">    Odjava</a></li>
          </ul>
        </div>
      </section>

      <section id="header">
        <a href="#"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="indexZaLog.php">NASLOVNA</a></li>
            <li><a class="active" href="otkup_admin.php">OTKUP</a></li>
            <li><a href="dodaj_proizvod.php">DODAJ PROIZVOD</a></li>
          </ul>
        </div>
      </section>

      <div style="padding-left: 40px;">

        <?php
            include "spoj.php";
            if(!empty($_SESSION['prijavljen']) && $_SESSION['prijavljen'] == true){
                    if($_SESSION['uloga'] == 'admin'){
                        echo "<br><p style='font-size:20px'>" ."ISPIS DODANIH PLOČA ZA OTKUP: ". "</p>";
                    }    
                        else{
                        echo "Prijavljeni ste kao kupac.";            
                    }                
            } else{
                header("Location: login.php");
                }
                $sql = "SELECT Ime,Prezime,Email,Ploca FROM ploce_za_otkup";
                $result = mysqli_query($conn, $sql);

                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    echo "<table><tr><th> IME </th><th> PREZIME </th><th> E-MAIL </th><th> PLOČA </th></tr>";
                    while($row = mysqli_fetch_assoc($result)) {
                        echo  "<tr> ";
                        echo "<tr><td>" .$row['Ime']. "</td><td>" .$row['Prezime']. "</td><td>" .$row['Email']. "</td><td>" .$row['Ploca']. "</td><td style='text-align: center; vertical-align: middle;'>" ."<a href='delete.php?delete_id=".$row['Email']."'><i class='fa fa-trash'></i></a>"."</td></tr>";
                        echo "</tr> ";
                    }
                    echo "</table>";
                } else {
                echo "0 results";
                }
                mysqli_close($conn);

            ?>
      </div>
    </body>
</html>




