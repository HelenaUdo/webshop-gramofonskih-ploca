<!doctype html>
<html lang="en">

 <head>
  <meta charset="utf-8">
  
  <title>Image Gallery</title>
  <meta name="description" content="Responsive Image Gallery">
  <meta name="author" content="Tim Wells">
  
  <link rel="stylesheet" href="stylesGallery.css?v=1.0">
  <link rel="stylesheet" href="style.css">

  <style>
	h1 {
  		font-family: 'Brush Script MT', cursive;
		text-align: center;
		font-size: 40px;
		padding-top: 25px;
		padding-bottom: 25px;
		text-shadow: 2px 2px 5px brown;
	}
	</style>
</head>

<body>

<section><h1>GALERIJA</h1></section>

<div id="gallery">
  
   <img src="images/galerija1.jpg">
   <img src="images/galerija2.jpg">
   <img src="images/galerija3.jpg">
   <img src="images/galerija4.jpg">
   <img src="images/galerija5.jpg">
   <img src="images/galerija6.jpg">
   <img src="images/galerija7.jpg">
   <img src="images/galerija8.jpg">
   <img src="images/galerija9.jpg">
   <img src="images/galerija10.jpg">
   <img src="images/galerija11.jpg">
   <img src="images/galerija12.jpg">
   <img src="images/galerija13.jpg">
   <img src="images/galerija14.jpg">
   <img src="images/galerija15.jpg">
   <img src="images/galerija16.jpg">
   <img src="images/galerija17.jpg">
  
</div>

  <script>
		let darkBoxVisible = false;

		window.addEventListener('load', (event) => {
			let images = document.querySelectorAll("img");
			if(images !== null && images !== undefined && images.length > 0) {
				images.forEach(function(img) {
					img.addEventListener('click', (evt) => {
						showDarkbox(img.src);
					});
				});
			}
		});

		function showDarkbox(url) {
			if(!darkBoxVisible) {
				let x = (window.innerWidth - 1280) / 2;
				let y = window.scrollY + 50;

				// Create the darkBox
				var div = document.createElement("div");
				div.id = "darkbox";
				div.innerHTML = '<img class="darkboximg" src="'+url+'" />';
				document.body.appendChild(div);
				let box = document.getElementById("darkbox");
				box.style.left = x.toString()+"px";
				box.style.top = y.toString()+"px";
				box.style.height = 'auto';
				box.addEventListener('click', (event) => {
					// Remove it
					let element = document.getElementById("darkbox");
					element.parentNode.removeChild(element);

					darkBoxVisible = false;
				})

				darkBoxVisible = true;

			} else {
				// Remove it
				let element = document.getElementById("darkbox");
				element.parentNode.removeChild(element);

				darkBoxVisible = false;
			}
		}
	</script>

<section class="section-p1">      
        <h2>Povratak na stranicu <a href="about.php" style="color:brown;">O NAMA</a></h2><br>
</section>

</body>
 
</html>