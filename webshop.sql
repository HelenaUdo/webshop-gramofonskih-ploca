-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2022 at 12:59 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `Ime` varchar(20) NOT NULL,
  `Prezime` varchar(20) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `K_ime` varchar(20) NOT NULL,
  `Lozinka` varchar(20) NOT NULL,
  `Uloga` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`Ime`, `Prezime`, `Email`, `K_ime`, `Lozinka`, `Uloga`) VALUES
('Ana', 'AniÄ‡', 'ana.ana@gmail.com', 'anita', 'ana123', 'korisnik'),
('Helena', 'Udovičić', 'mojmail@gmail.com', 'helenaudo', 'gorilampa', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `ploce_za_otkup`
--

CREATE TABLE `ploce_za_otkup` (
  `Ime` varchar(20) NOT NULL,
  `Prezime` varchar(20) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Ploca` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci;

--
-- Dumping data for table `ploce_za_otkup`
--

INSERT INTO `ploce_za_otkup` (`Ime`, `Prezime`, `Email`, `Ploca`) VALUES
('Ivo', 'Bego', 'ivo.bego@gmail.com', 'BonnieM-Pjesma'),
('Nina', 'Pego', 'nina.pego@gmail.com', 'Nina-Dani');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`K_ime`);

--
-- Indexes for table `ploce_za_otkup`
--
ALTER TABLE `ploce_za_otkup`
  ADD PRIMARY KEY (`Email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
