<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>O nama</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

        <style>
          a:link {
            text-decoration: none;
          }

          a:visited {
            text-decoration: none;
          }

          a:hover {
            text-decoration: none;
            color: #088178;
          }

          a:active {
            text-decoration: none;
            color: #088178;
          }
          a{
            color: brown;
          }
        </style>

    </head>

    <body>
        <?php
            include "spoj.php";
            session_start();
            if ($_SESSION == NULL) {
        ?>
        <section id="firsthead">
          <div>
            <ul id="firstnavbar">
              <li><a href="register.php">Registracija</a></li>
              <li><a href="login.php">Prijava</a></li>
            </ul>
          </div>
        </section>
        <?php  
        } else {
        ?>
        <section id="firsthead">
          <div>
              <ul id="firstnavbar">
                <li><a href="odjava.php">Odjava</a></li>
              </ul>
          </div>
        </section>
        <?php
        }
        ?>

      <section id="header">
        <a href="index.php"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="index.php">NASLOVNA</a></li>
            <li><a href="shop.php">TRGOVINA</a></li>
            <li><a href="otkup.php">OTKUP</a></li>
            <li><a class="active" href="about.php">O NAMA</a></li>
            <li id="lgbag"><a href="cart.php"><i class="far fa-shopping-bag"></i><span> 0</span></a></li>
            <a href="#" id="closeit"><i class="far fa-times"></i></a>
          </ul>
        </div>

        <div id="mobile">
          <i id="bar" class="fas fa-outdent"></i>
        </div>
      </section>

      <section id="insideshop">
      </section>

      <section id="onama_opis" class="section-p1">
        <div class="prviopis">
            <h2 style="color:brown;"><b>NAŠA PRIČA</b></h2>
        </div>
        <div class="drugiopis">
            <p>Music Premium nastao je iz entuzijazma dvoje zaljubljenika u gramofonske ploče Tonija i Tatjane.<br>
               Sama ideja o otvaranju prodavaonice ploča u Dubravi bila je Tonijeva.<br> 
               Nakon kratke prezentacije supruzi i njezine oduševljene reakcije krenuo je brainstorming i ta je ideja dobila novu dimenziju.<br> 
               Odlučili smo dodati i mnoštvo dodatnih stvari osim samih ploča i na taj način objediniti sve naše interese.<br> 
               Odluka je pala u sekundi i bez razmišljanja, to je bilo apsolutno “da” s uskličnikom. Nakon toga zasukali smo rukave i krenuli.<br> 
               Prvi je korak bio davanje otkaza u firmi gdje je Toni radio, zatim nabava ploča, traženje i uređenje prostora i jos štošta.<br> 
               Uz veliku pomoć naših predivnih i nesebičnih roditelja i prijatelja, ideja i želja o otvaranju vinil shopa se ostvarila.</p>
        </div>
      </section>
      <section id="onama_opis2" class="section-p1">
        <div class="prviopis">
            <h2 style="color:brown;"><b>GLAZBA ZA SVA OSJETILA</b></h2>
        </div>
        <div class="drugiopis">
            <p>Moto nam je „Glazba za sva osjetila“. To opravdavamo time što u ponudi imamo glazbu na gramofonskim pločama,<br>
               tj. naša glazba je i opipljiva za razliku od digitalnih izdanja te zadovoljava i osjet opipa, ne samo sluha.<br> 
               U prodavaonici možete provesti ugodno vrijeme preslušavajući ploče, ali i čitajući glazbenu literaturu.<br>
               Ovdje možete dobiti i Quahwa kavu u više različitih okusa.<br> 
               Sve njihove kave su aromatizirane raznim dodacima i okusima na taj način nudimo i cijelo iskustvo za sva osjetila njuh i okus.<br>  
               Za osjetilo vida pobrinula se naša predivna prijateljica i umjetnica, koja je oslikala naš izlog i zid<br> 
               Zidove nam ukrašavaju i satovi izrađeni od neispravnih gramofonskih ploča.<br>
               Surađujemo i s lokalnim umjetnicima čija djela, koja su vezana uz glazbu, naši kupci mogu pogledati u vidu mini izložbe.<br>
               Falilo nam je neko mjesto u kvartu gdje bismo mogli uživati u glazbi te je podijeliti s drugima.<br> 
               Tako da smo na kraju došli do zaključka da ćemo takvo mjesto otvoriti sami.<br>
               Našu galeriju slika možete vidjeti ovdje: <a href="galerija.php">GALERIJA</a></p>
        </div>
      </section>

      <?php
      include "footer.php";
      ?>
      
      <script src="script.js"></script>
    </body>


</html>