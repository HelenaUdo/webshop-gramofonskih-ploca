<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="Width-device-width, initial-scale=1.0">
        <title>Otkup</title>
        <link rel="stylesheet" href="style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" />

    </head>

    <body>
    <?php
          include "spoj.php";
          if ($_SESSION == NULL) {
      ?>
      <section id="firsthead">
        <div>
          <ul id="firstnavbar">
            <li><a href="register.php">Registracija</a></li>
            <li><a href="login.php">Prijava</a></li>
          </ul>
        </div>
      </section>
      <?php  
      } else {
      ?>
      <section id="firsthead">
        <div>
            <ul id="firstnavbar">
              <li><a href="odjava.php">Odjava</a></li>
            </ul>
        </div>
      </section>
      <?php
      }
      ?>

      <section id="header">
        <a href="index.php"><img src="images/logo-retro-gramophone.jpg" style="width: 150px; height: 150px" alt="logo"></a>
        <h3>Music Premium</h3>
      </section>

      <section id="head">
        <div>
          <ul id="navbar">
            <li><a href="index.php">NASLOVNA</a></li>
            <li><a href="shop.php">TRGOVINA</a></li>
            <li><a class="active" href="otkup.php">OTKUP</a></li>
            <li><a href="about.php">O NAMA</a></li>
            <li id="lgbag"><a href="cart.php"><i class="far fa-shopping-bag"></i><span> 0</span></a></li>
            <a href="#" id="closeit"><i class="far fa-times"></i></a>
          </ul>
        </div>

        <div id="mobile">
          <i id="bar" class="fas fa-outdent"></i>
        </div>
      </section>

      <section id="insideshop">
      </section>


      <?php 
        require_once "spoj.php";

        if(isset($_POST['email'])){
            unset($error);

            $sql = 'SELECT * FROM ploce_za_otkup WHERE Email = "'.$email.'"';
            $result = mysqli_query($conn,$sql);
            if($result->num_rows > 0){
                $error = "Email zauzet!<br>";
            }
            else{   
                $ime = $_POST['ime'];
                $prezime = $_POST['prezime'];
                $email = $_POST['email'];
                $ploca = $_POST['ploca'];
                    
                $sql = "INSERT INTO ploce_za_otkup (Ime,Prezime,Email,Ploca) VALUES('$ime','$prezime','$email','$ploca')";
                if ($conn->query($sql)){
                    header("location: otkup.php");
                }
                else {
                    echo "Error: " . $sql . ": -" . mysqli_error($conn);
                }
                mysqli_close($conn);
                echo "<h2>Vaši su podaci uspješno poslani</h2>";
            }
        }
        ?>


      <section id="otkup_opis" class="section-p1">
        <div class="prviopis">
            <h2>OTKUPLJUJEMO VAŠE GRAMOFONSKE PLOČE</h2>
        </div>
        <div class="drugiopis">
            <p>U svojim podrumima, ostavama, šupama i tavanima imate gramofonske ploče koje skupljaju prašinu i samo Vam smetaju?
            Mi bismo ih rado otkupili od Vas i našli im novi dom.<br>
            Otkupljujemo gramofonske ploče svih formata koje su u dobrom stanju te imaju pripadajuće omote. 
            Cijena ovisi o stanju ploče, izdanju, izvođaču i naslovu.<br>
            Procjena se može obaviti u našem dućanu ili na Vašoj adresi.<br>
            Kontaktirajte nas putem obrasca ili na broj mobitela: 091/8863-328.</p>
        </div>
      </section>

      <section id="otkup_unos" class="section-p1">
        <form action="" method="post">
            <span>UNESITE SVOJE PODATKE KAKO BISMO VAS MOGLI KONTAKTIRATI</span>
            <input type="text" name="ime" id="ime" placeholder="Ime">
            <input type="text" name="prezime" id="prezime" placeholder="Prezime">
            <input type="text" name="email" id="email" placeholder="E-mail">
            <input type="text" name="ploca" id="ploca" placeholder="Naziv ploče">
            <button class="send_normal" id="otkupise">Pošalji</button><br><br>
        </form>
      </section>

      <?php
      include "footer.php";
      ?>

      <script src="script.js"></script>
    </body>


</html>